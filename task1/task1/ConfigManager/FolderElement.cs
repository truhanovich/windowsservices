﻿using System.Configuration;

namespace task1.ConfigManager
{
    public class FolderElement : ConfigurationElement
    {

        [ConfigurationProperty("searchType", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string searchType
        {
            get { return ((string)(base["searchType"])); }
            set { base["searchType"] = value; }
        }

        [ConfigurationProperty("path", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string Path
        {
            get { return ((string)(base["path"])); }
            set { base["path"] = value; }
        }
    }
}
